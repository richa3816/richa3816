# About me

Hello all! I am currently a computer-science student and am looking to get into
systems programming, something low-level, maybe something
safety-/mission-critical.


I'm currently practicing FPGA engineering and C, but am also learning new
things from all disciplines all the time. You'll probably find these projects
between all the miscellaneous (and more important) repos like my dotfiles.
It'll be cluttered because I wouldn't put it in the cloud if I wanted it to be
private ;)

## TODO

- [ ] Expression evaluator in C
- [ ] Neovim plugin—tiled live markdown renderer
- [ ] Trusty YAMG

# About this account

My projects are between this forge and my GitHub of the same name, maybe
something new in the future too! Expect this to be my main forge, but there are
no guarantees from me.
